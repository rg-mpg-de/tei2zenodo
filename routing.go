package main

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/stephenmuss/ginerus"
)

// SetupRoutes establishes the various API endpoints.
func SetupRoutes(conf Config) *gin.Engine {

	// Switch gin to release mode
	gin.SetMode(gin.ReleaseMode)

	// Create a gin router with logrus router and stock recovery
	router := gin.New()
	router.Use(ginerus.Ginerus(), gin.Recovery(), cors.Default())
	router.LoadHTMLGlob("templates/*")

	// Routes - one for the html/webapp, one group per API version...
	metadataConf, _ := json.MarshalIndent(conf.Metadata, "", "\t")
	secretConf := (conf.Secret != "")
	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", gin.H{
			"metadataConf": string(metadataConf),
			"secretConf":   bool(secretConf),
		})
		// page := c.DefaultQuery("p", "index")
		// c.String(http.StatusOK, "Service homepage with page "+page+".")
	})
	router.GET("/index.html", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", gin.H{
			"metadataConf": string(metadataConf),
		})
	})

	APIv1 := router.Group(conf.APIRoot)
	{
		if conf.FileAPI != "" {
			APIv1.POST(conf.FileAPI, func(c *gin.Context) {

				var r io.ReadSeeker
				var myDeposit Deposit
				var DOIs []string

				if conf.Verbose {
					log.Printf("====== Received POST message from %s at %s ======", c.Request.RemoteAddr, conf.FileAPI)
				} else {
					log.Printf("====== Received POST message ======")
				}

				// Do a first parse of the request (is it a form?)
				_, err := c.MultipartForm()
				if err != nil {
					log.Tracef("  Request could not be parsed as multipart form. Maybe a raw upload?")
				}

				// === I. Validate - token, signature etc. ===

				// Read zenodo token from Form field or Query parameter
				ztoken := c.PostForm("ztoken")
				if ztoken == "" {
					ztoken = c.Query("ztoken")
				}
				if ztoken == "" {
					log.Errorf("POST request without token submitted. Too insecure - ignoring...")
					AbortMsg(401, NewError("errBadRequest", "POST request without token submitted. Too insecure - ignoring...", 401, nil), c)
					return
				}
				conf.Zenodo.Token = ztoken

				// Read filename from Form field, Query parameter or File field
				filename := c.PostForm("filename")
				if filename == "" {
					filename = c.Query("filename")
				}
				// Read file from Form or as raw body
				ff, err := c.FormFile("file")
				if err != nil {
					log.Debugf("  Request form did not contain a file. Maybe a raw upload?")
					buf := new(bytes.Buffer)
					length, err := buf.ReadFrom(c.Request.Body)
					if length == 0 || err != nil {
						log.Errorf("Problem reading raw upload: %+v", err)
						AbortMsg(400, NewError("errParse", fmt.Sprintf("problem reading raw upload: %s", err), 400, err), c)
						return
					}
					file := buf.String()
					r = strings.NewReader(file)
				} else {
					if filename == "" {
						filename = ff.Filename
					}
					file, err := ff.Open()
					if err != nil {
						log.Errorf("Problem opening form file: %+v", err)
						AbortMsg(400, NewError("errParse", fmt.Sprintf("problem opening form file: %s", err), 400, err), c)
						return
					}
					r = file
				}

				// If Secret is configured, check signature
				if conf.Secret != "" {
					// Read signature from form field or query parameter
					signature := c.PostForm("signature")
					if signature == "" {
						signature = c.Query("signature")
					}

					messagebytes, err := streamToByte(r)
					if err != nil {
						AbortMsg(500, NewError("errWebhook", "Could not convert file to bytes", 500, nil), c)
					}
					result := ValidMAC(messagebytes, []byte("sha1="+signature), []byte(conf.Secret))
					if !result {
						if conf.Verbose {
							mac := hmac.New(sha1.New, []byte(conf.Secret))
							mac.Write(messagebytes)
							expectedMAC := hex.EncodeToString(mac.Sum(nil))
							log.Warnf("File upload request with invalid HMAC signature received, ignoring. Sign.: %s, Message hash: %s", signature, expectedMAC)
						} else {
							log.Warnf("File upload request with invalid HMAC signature received, ignoring")
						}
						AbortMsg(403, NewError("errWebhook", "File upload request with invalid HMAC signature received, ignoring", 403, nil), c)
						return
					}
					log.Debugf("  File upload request HMAC signature verified")
					_, _ = r.Seek(0, 0)
				}

				// If we still have no filename, parse one from the file's contents
				if filename == "" {
					log.Warnf("  No filename specified. Creating one from the file's content...")
					f, GFErr := GetFilename(r)
					if GFErr != nil {
						log.Errorf("Problem reading filename: %+v", GFErr)
						AbortMsg(500, NewError("errParse", fmt.Sprintf("error reading filename: %s", GFErr), 500, GFErr), c)
						return
					}
					filename = f
					log.Debugf("  Set filename to %s.", filename)
					_, _ = r.Seek(0, 0)
				}
				myDeposit.Filename = filename

				// === II. WriteDOI settings ===
				// Allow to overwrite config settings with form field or query parameter

				// Read writeDOImode from Form field, Query parameter or File field
				writeDOImode := c.PostForm("writeDOImode")
				if writeDOImode == "" {
					writeDOImode = c.Query("writeDOImode")
				}
				if writeDOImode != "" {
					conf.WriteDOI.Mode = writeDOImode
				}

				// Read writeDOIparentPath from Form field, Query parameter or File field
				writeDOIparentPath := c.PostForm("writeDOIparentPath")
				if writeDOIparentPath == "" {
					writeDOIparentPath = c.Query("writeDOIparentPath")
				}
				if writeDOIparentPath != "" {
					conf.WriteDOI.ParentPath = writeDOIparentPath
				}

				// Read writeDOIposition from Form field, Query parameter or File field
				writeDOIposition := c.PostForm("writeDOIposition")
				if writeDOIposition == "" {
					writeDOIposition = c.Query("writeDOIposition")
				}
				if writeDOIposition != "" {
					conf.WriteDOI.Position = writeDOIposition
				}

				// Read writeDOIelementType from Form field, Query parameter or File field
				writeDOIelementType := c.PostForm("writeDOIelementType")
				if writeDOIelementType == "" {
					writeDOIelementType = c.Query("writeDOIelementType")
				}
				if writeDOIelementType != "" {
					conf.WriteDOI.ElementType = writeDOIelementType
				}

				// Read other Attribute definitions from Form field, Query parameter or File field
				writeDOIotherAttName := c.PostForm("writeDOIotherAttName")
				if writeDOIotherAttName == "" {
					writeDOIotherAttName = c.Query("writeDOIotherAttName")
				}
				writeDOIotherAttValue := c.PostForm("writeDOIotherAttValue")
				if writeDOIotherAttValue == "" {
					writeDOIotherAttValue = c.Query("writeDOIotherAttValue")
				}
				if writeDOIotherAttName != "" {
					otherAtt := AttributeSpec{AttName: writeDOIotherAttName, Value: writeDOIotherAttValue}
					conf.WriteDOI.OtherAttributes = append(conf.WriteDOI.OtherAttributes, otherAtt)
				}

				// Read writeDOIattributeName from Form field, Query parameter or File field
				writeDOIattributeName := c.PostForm("writeDOIattributeName")
				if writeDOIattributeName == "" {
					writeDOIattributeName = c.Query("writeDOIattributeName")
				}
				if writeDOIattributeName != "" {
					conf.WriteDOI.AttributeName = writeDOIattributeName
				}

				// === III. Zenodo settings ===
				// Allow to overwrite config settings with form field or query parameter

				// Read zURL from Form field, Query parameter or File field
				zURL := c.PostForm("zURL")
				if zURL == "" {
					zURL = c.Query("zURL")
				}
				if zURL != "" {
					conf.Zenodo.Host = zURL
				}

				// Read zPrefix from Form field, Query parameter or File field
				zPrefix := c.PostForm("zPrefix")
				if zPrefix == "" {
					zPrefix = c.Query("zPrefix")
				}
				if zPrefix != "" {
					conf.Zenodo.Prefix = zPrefix
				}

				// Get doPublish from request (false if not set)
				doPublish := c.PostForm("doPublish")
				if doPublish == "" {
					doPublish = c.Query("doPublish")
				}
				if doPublish == "True" {
					myDeposit.DoPublish = true
				} else {
					myDeposit.DoPublish = false
				}

				// === IV. Get file metadata / Parse TEI file ===

				// Parse TEI file
				var md ZMetadata
				doi, PTErr := ParseTEI(r, &md, &conf)
				if PTErr != nil {
					switch PTErr.Typ {
					case "errNoTEIXML":
						{
							log.Warnf("Problem with file %s: No TEI file.", myDeposit.Filename)
							AbortMsg(400, NewError("errZProcessing", fmt.Sprintf("problem with file %s: No TEI file.", myDeposit.Filename), 400, PTErr), c)
							return
						}
					default:
						{
							log.Errorf("Error parsing TEI file: %v", PTErr)
							AbortMsg(500, NewError("errParse", fmt.Sprintf("error parsing TEI file %s (%s): %s", myDeposit.Filename, myDeposit.GithubObjSHA, PTErr.Error()), 500, PTErr), c)
							return
						}
					}
				}
				_, _ = r.Seek(0, 0)
				md.DOI = ""

				// Before processing, nothing has changed
				myDeposit.Changed = false

				// === V. Process file: claim/mixin doi, upload file ===

				ZPFErr := ProcessFile(r, doi, &md, &myDeposit, &conf)
				if ZPFErr != nil {
					log.Errorf("Problem processing file %s: %+v", myDeposit.Filename, ZPFErr)
					AbortMsg(500, NewError("errZProcessing", fmt.Sprintf("problem processing file %s: %s", myDeposit.Filename, ZPFErr.Error()), 500, ZPFErr), c)
					return
				}

				log.Printf("  Successfully processed file %s (DOI %s).", myDeposit.Filename, myDeposit.DepositDOI)
				DOIs = append(DOIs, myDeposit.DOIURL)

				log.Printf("====== All done ======")
				if len(DOIs) == 1 {
					c.Header("Location", DOIs[0])
				}
				if myDeposit.Changed {
					c.Header("Content-Description", "File Transfer")
					c.Header("Content-Disposition", "attachment; filename="+myDeposit.Filename)
					c.Data(http.StatusOK, "application/tei+xml", []byte(myDeposit.FileContent))
					// c.XML(http.StatusOK, myDeposit.FileContent)
				} else {
					c.JSON(http.StatusCreated, gin.H{"doi": DOIs})
				}
			})
		}
		if conf.WebhookAPI != "" {
			APIv1.POST(conf.WebhookAPI, func(c *gin.Context) {

				var r io.ReadSeeker

				// Read hook type from header
				hookType := c.Request.Header.Get("X-GitHub-Event")
				if conf.Verbose {
					log.Printf("====== Received %s message from %s at %s ======", hookType, c.Request.RemoteAddr, conf.WebhookAPI)
				} else {
					log.Printf("====== Received %s message ======", hookType)
				}

				// Read payload from Request body
				buf := new(bytes.Buffer)
				length, err := buf.ReadFrom(c.Request.Body)
				if length == 0 || err != nil {
					log.Errorf("Problem reading raw upload: %+v", err)
					AbortMsg(400, NewError("errParse", fmt.Sprintf("problem reading raw upload: %s", err), 400, err), c)
					return
				}
				file := buf.String()
				r = strings.NewReader(file)

				// If Secret is configured, check signature
				if conf.Secret != "" {
					signature := c.Request.Header.Get("X-Hub-Signature")
					result := ValidMAC([]byte(file), []byte(signature), []byte(conf.Secret))
					if !result {
						if conf.Verbose {
							mac := hmac.New(sha1.New, []byte(conf.Secret))
							mac.Write([]byte(file))
							expectedMAC := []byte("sha1=" + hex.EncodeToString(mac.Sum(nil)))
							log.Warnf("Webhook with invalid HMAC signature received, ignoring. Sign.: %s, Message hash: %s", signature, expectedMAC)
						} else {
							log.Warnf("Webhook with invalid HMAC signature received, ignoring")
						}
						AbortMsg(403, NewError("errWebhook", "Webhook with invalid HMAC signature received, ignoring", 403, nil), c)
						return
					}
					log.Debugf("  Webhook HMAC signature verified")
				}

				// Process Hook to extract files
				files, PHErr := ProcessHook(hookType, r, &conf)
				if PHErr != nil {
					switch PHErr.Typ {
					case "warnInapplicable":
						{
							log.Warnf("webhook inapplicable: %v", PHErr)
							c.Status(http.StatusNoContent) // "processed hook successfully (nothing to do)"
							return
						}
					default:
						{
							log.Errorf("Error parsing webhook event: %v", PHErr)
							AbortMsg(500, NewError("errWebhook", fmt.Sprintf("problem parsing webhook for %s event", hookType), 500, PHErr), c)
							return
						}
					}
				}

				var DOIs []string
				var SHAs []string

				// Send each file to processing (parsing, mixing, uploading)
				for f := range files {
					log.Printf("--- Process file '%s' ---", f)

					var myDeposit Deposit

					// myDeposit.Filename = strings.Replace(f, "/", "_", -1)
					myDeposit.Filename = f
					myDeposit.CommitSHA = files[f].CommitSHA
					myDeposit.DoPublish = files[f].DoPublish
					myDeposit.GithubBlobURL = files[f].BlobURL
					myDeposit.GithubObjSHA = files[f].ObjectSHA
					myDeposit.GithubRawURL = files[f].RawURL

					// Get file
					r, GDLErr := DownloadFile(&myDeposit, &conf)
					if GDLErr != nil {
						log.Errorf("Problem downloading file %s (%s): %v ...", myDeposit.Filename, myDeposit.GithubObjSHA, GDLErr)
						AbortMsg(500, NewError("errNetComm", fmt.Sprintf("error downloading file %s (%s): %s", myDeposit.Filename, myDeposit.GithubObjSHA, GDLErr.Error()), 500, GDLErr), c)
						return
					}

					// Parse TEI file
					var md ZMetadata
					doi, PTErr := ParseTEI(r, &md, &conf)
					if PTErr != nil {
						switch PTErr.Typ {
						case "errNoTEIXML":
							{
								log.Warnf("Problem with file %s (%s): No TEI file.", myDeposit.Filename, myDeposit.GithubObjSHA)
								// AbortMsg(400, NewError("errZProcessing", fmt.Sprintf("problem with file %s (%s): No TEI file.", myDeposit.Filename, myDeposit.GithubObjSHA), 400, PTErr), c)
								continue
							}
						default:
							{
								log.Errorf("Error parsing TEI file: %v", PTErr)
								AbortMsg(500, NewError("errParse", fmt.Sprintf("error parsing TEI file %s (%s): %s", myDeposit.Filename, myDeposit.GithubObjSHA, PTErr.Error()), 500, PTErr), c)
								return
							}
						}
					}
					_, _ = r.Seek(0, 0)
					md.DOI = ""

					// Before processing, nothing has changed
					myDeposit.Changed = false

					// Upload (and publish?) file to zenodo
					ZPFErr := ProcessFile(r, doi, &md, &myDeposit, &conf)
					if ZPFErr != nil {
						log.Errorf("Problem processing file %s: %v ...", myDeposit.Filename, ZPFErr)
						AbortMsg(500, NewError("errZProcessing", fmt.Sprintf("problem processing file %s (%s): %s", myDeposit.Filename, myDeposit.GithubObjSHA, ZPFErr.Error()), 500, ZPFErr), c)
						return
					}

					// Upload file back to github
					newCommitSHA, GPFErr := PutFile(&myDeposit, &conf, c)
					if GPFErr != nil {
						log.Errorf("Error putting file to github: %v", GPFErr)
						AbortMsg(500, NewError("errPutFile", fmt.Sprintf("problem putting file to github: %v", f), 500, GPFErr), c)
						return
					}

					log.Printf("  Successfully processed file %s (DOI %s, github SHA %s).", myDeposit.Filename, myDeposit.DepositDOI, myDeposit.CommitSHA)
					myDeposit.CommitSHA = newCommitSHA

					DOIs = append(DOIs, myDeposit.DOIURL)
					SHAs = append(SHAs, "https://api.github.com/repos/"+conf.Git.Repo+"/commits/"+myDeposit.CommitSHA)
				}

				log.Printf("====== All done ======")
				if len(DOIs) == 1 {
					c.Header("Location", DOIs[0])
				}
				c.JSON(http.StatusCreated, gin.H{"doi": DOIs, "commits": SHAs})
			})
		}
	}
	return router
}

// AbortMsg returns an error code and message.
func AbortMsg(code int, err *Error, c *gin.Context) {
	// A custom error page with HTML templates can be shown by c.HTML()
	/*
		c.String(code, "Something has gone wrong, causing a %v error.\n", code)
			if err != nil {
			c.Error(err)
		}
	*/
	c.JSON(code, gin.H{"type": err.Typ, "title": err.Message, "detail": err.ErrorVal, "status": code})
	c.Abort()
}

// ValidMAC reports whether messageMAC is a valid HMAC tag for message.
func ValidMAC(message, messageMAC, key []byte) bool {
	mac := hmac.New(sha1.New, key)
	mac.Write(message)
	expectedMAC := []byte("sha1=" + hex.EncodeToString(mac.Sum(nil)))
	return hmac.Equal(messageMAC, expectedMAC)
}

func streamToByte(stream io.Reader) ([]byte, error) {
	buf := new(bytes.Buffer)
	length, err := buf.ReadFrom(stream)
	if err != nil {
		log.Errorf("Error reading from stream: %v", err)
		return nil, err
	}
	if length == 0 {
		return nil, fmt.Errorf("Input stream empty")
	}
	return buf.Bytes(), nil
}
