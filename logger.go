package main

import (
	"io"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
)

// ConfigureLogging will take the logging configuration and also adds a few default parameters.
func ConfigureLogging(config *LoggingConfig) (*logrus.Logger, error) {
	/*
		hostname, err := os.Hostname()
		if err != nil {
			return nil, err
		}
	*/

	// use a file if you want
	if config.File != "" {
		f, errOpen := os.OpenFile(config.File, os.O_CREATE|os.O_RDWR|os.O_APPEND, 0660)
		if errOpen != nil {
			return nil, errOpen
		}
		// logrus.SetOutput(bufio.NewWriter(f))
		logrus.SetOutput(io.MultiWriter(f, os.Stdout))
	}

	if config.Level != "" {
		l, errParse := logrus.ParseLevel(strings.ToUpper(config.Level))
		if errParse != nil {
			return nil, errParse
		}
		logrus.SetLevel(l)
	}

	// always use the fulltimestamp
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp:    true,
		DisableTimestamp: false,
	})

	return logrus.StandardLogger(), nil
}
