package main

// tei2zenodo service parses TEI files for metadata and published them on zenodo.
// Files can be fed via REST POST or by calling a webhook that will retrieve the file(s).

import (
	"bytes"
	"fmt"
	"os/exec"
	"strconv"
	"strings"

	"github.com/gin-gonic/autotls"
	"github.com/sirupsen/logrus"
)

// Config stores the application's configuration.
var log *logrus.Logger

func main() {
	var Config Config
	Cerr := Configure(&Config)
	if Cerr != nil {
		log.Fatal(fmt.Errorf("Error during config phase_ %+v", Cerr))
	}

	var Lerr error
	log, Lerr = ConfigureLogging(&Config.Log)
	if Lerr != nil {
		log.Fatal(fmt.Errorf("Error during log setup phase: %+v", Lerr))
	}

	log.Printf("Starting tei2zenodo daemon")
	if Config.Verbose {
		log.Printf("conf: %+v", Config)
	} else {
		log.Tracef("conf: %+v", Config)
	}

	// Get routes
	router := SetupRoutes(Config)

	// Run server, taking into account different SSL/TLS approaches
	switch Config.HTTPS {
	case "letsencrypt":
		{
			fqdn, _ := getHostnameFQDN()
			log.Fatal(autotls.Run(router, fqdn))
		}
	case "none":
		{
			log.Fatal(router.Run(":" + strconv.FormatInt(Config.ListenSpec, 10)))
		}
	default:
		{
			certfile := strings.Split(Config.HTTPS, ":")[0]
			keyfile := strings.Split(Config.HTTPS, ":")[1]
			log.Fatal(router.RunTLS(":"+strconv.FormatInt(Config.ListenSpec, 10), certfile, keyfile))
		}
	}
}

func getHostnameFQDN() (string, error) {
	cmd := exec.Command("/bin/hostname", "-f")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return "", fmt.Errorf("Error when get_hostname_fqdn: %v", err)
	}
	fqdn := out.String()
	fqdn = fqdn[:len(fqdn)-1] // removing EOL

	return fqdn, nil
}
