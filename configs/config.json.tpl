{
    "listenSpec": 8081,
    "https": "none",
    "verbose": false,
    "apiRoot": "/api/v1",
    "fileAPI": "/file",
    "webhookAPI": "/hooks/receivers/github/events/",
    "secret": "ThIs_iS_NoT_ReAlLy_a_sMaRt_sEcReT!!",
    "writeDOI": {
        "mode": "add",
        "parentPath": "/TEI/teiHeader/fileDesc/publicationStmt",
        "elementType": "idno",
        "otherAttributes": [
            {
                "attName": "type",
                "value": "DOI"
            }
        ],
        "position": "firstChild",
        "attributeName": "",
        "addChangeDesc": true
    },
    "zenodo": {
        "token": "aBcDeFgHiJkLmNoPqRsTuVwXyZ",
        "host": "https://sandbox.zenodo.org",
        "prefix": "10.5072/zenodo."
    },
    "git": {
        "token": "aBcDeFgHiJkLmNoPqRsTuVwXyZ",
        "host": "https://api.github.com",
        "repo": "octocat/hello-world",
        "branch": "public",
        "hookUser": "foobar",
        "triggerPhrase": "(push to zenodo)",
        "dontPublishPhrase": "test"
    },
    "metadata": {
        "fields": [
            {
                "field": "upload_type",
                "xExpression": "string('publication')"
            },
            {
                "field": "publication_type",
                "xExpression": "string('other')"
            },
            {
                "field": "publication_date",
                "xPath": "//publicationStmt/date"
            },
            {
                "field": "title",
                "xPath": "//titleStmt//title[@type='main']"
            },
            {
                "field": "creators",
                "xPath": "//titleStmt/author",
                "subfields": [
                    {
                        "field": "name",
                        "xPath": "."
                    },
                    {
                        "field": "affiliation",
                        "xPath": ""
                    },
                    {
                        "field": "orcid",
                        "xPath": ""
                    },
                    {
                        "field": "gnd",
                        "xPath": ""
                    }
                ]
            },
            {
                "field": "description",
                "xExpression": "string('One of the seminal works published in the context of the project XYZ.')"
            },
            {
                "field": "access_right",
                "xExpression": "string('open')"
            },
            {
                "field": "license",
                "xPath": "//publicationStmt/availability/licence/@n"
            },
            {
                "field": "contributors",
                "xPath": "//titleStmt/editor",
                "subfields": [
                    {
                        "field": "name",
                        "xPath": "."
                    },
                    {
                        "field": "type",
                        "xPath": "@role"
                    },
                    {
                        "field": "affiliation",
                        "xPath": ""
                    },
                    {
                        "field": "orcid",
                        "xPath": ""
                    },
                    {
                        "field": "gnd",
                        "xPath": ""
                    }
                ]
            },
            {
                "field": "doi",
                "xPath": "//publicationStmt//idno[@type='DOI']"
            },
            {
                "field": "keywords",
                "xPath": "//teiHeader/profileDesc/textClass/keywords/term"
            }
        ]
    },
    "log": {
        "file": "t2z.log",
        "level": "Info"
    }
}
