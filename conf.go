package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/viper"
)

// Data Types

type Config struct {
	ListenSpec       int64          `json:"listenSpec"`
	HTTPS            string         `json:"https"`
	Verbose          bool           `json:"verbose"`
	APIRoot          string         `json:"apiRoot"`
	FileAPI          string         `json:"fileAPI"`
	WebhookAPI       string         `json:"webhookAPI"`
	Secret           string         `json:"secret"`
	WriteDOI         WriteDOIConfig `json:"writeDOI"`
	Zenodo           ZenodoConfig   `json:"zenodo"`
	Git              GitConfig      `json:"git"`
	Metadata         MetadataConfig `json:"metadata"`
	Log              LoggingConfig  `json:"log"`
	T2ZCommitMessage string         `json:"t2zCommitMessage,omitempty"`
}

// WriteDOIConfig specifies if and how a new DOI value is written to the TEI file before processing
type WriteDOIConfig struct {
	Mode            string          `json:"mode"` // 'add' or 'replace' (which actually also adds the element in question if none is present)
	ParentPath      string          `json:"parentPath"`
	ElementType     string          `json:"elementType"`
	OtherAttributes []AttributeSpec `json:"otherAttributes"` // if we need to add more attributes to an element that we're creating
	Position        string          `json:"position"`        // 'firstChild' or 'lastChild'
	AttributeName   string          `json:"attributeName"`   // if the DOI goes into an attribute rather than a text node
	AddChangeDesc   bool            `json:"addChangeDesc"`
}

// AttributeSpec specifies an (xml) attribute and its value
type AttributeSpec struct {
	AttName string `json:"attName"`
	Value   string `json:"value"`
}

// ZenodoConfig specifies parameters for the zenodo repository connection.
type ZenodoConfig struct {
	Host   string `json:"host"`
	Token  string `json:"token"`
	Prefix string `json:"prefix"`
}

// GitConfig specifies git repositories from which webhooks are accepted.
type GitConfig struct {
	Host              string `json:"host"`
	Token             string `json:"token"`
	Repo              string `json:"repo"`
	Branch            string `json:"branch"`
	HookUser          string `json:"hookUser"`
	TriggerPhrase     string `json:"triggerPhrase"`
	DontPublishPhrase string `json:"dontPublishPhrase"`
}

// MetadataConfig specifies metadata fields and xpaths to retrieve their values if the need to be different from the defaults.
type MetadataConfig struct {
	Fields []metadataField `json:"fields"`
}

type metadataField struct {
	Field       string          `json:"field"`
	XPath       string          `json:"xPath"`
	XExpression string          `json:"xExpression"`
	Subfields   []metadataField `json:"subfields"`
}

// LoggingConfig specifies all the parameters needed for logging.
type LoggingConfig struct {
	File  string `json:"file"`
	Level string `json:"level"`
}

// Error messages
type Error struct {
	Typ      string // errNoTEIXML, errParse, errWebhook, errZProcessing, errGHProcessing, errSerializing, errInternal, errBadConfig, errNetComm, errBadRequest, warnInapplicable
	Message  string
	HTTPCode int
	ErrorVal error // what may have been raised by previous code
}

// NewError returns a pointer to a Error struct
func NewError(typ string, msg string, status int, err error) *Error {
	return &Error{
		Typ:      typ,
		Message:  msg,
		ErrorVal: err,
		HTTPCode: status,
	}
}

// Error() method of interface
func (e *Error) Error() string {
	return e.Message
}

// Configure loads configuration parameters into Config struct
func Configure(Config *Config) error {

	/* Enable commandline switches:
	err := viper.BindPFlags(cmd.Flags())
	if err != nil {
		return err
	}
	*/

	// set defaults
	viper.SetDefault("ListenSpec", "8081")
	viper.SetDefault("HTTPS", "none")
	viper.SetDefault("Verbose", false)
	viper.SetDefault("APIRoot", "/api/v1")
	viper.SetDefault("FileAPI", "/file")
	viper.SetDefault("WebhookAPI", "/hooks/receivers/github/events/")

	viper.SetDefault("Log", map[string]string{"file": "t2z.log", "level": "Info"})

	viper.SetDefault("Zenodo", map[string]string{"prefix": "10.5072/zenodo.",
		"host":  "https://sandbox.zenodo.org",
		"token": "",
	})

	viper.SetDefault("Git", map[string]string{"host": "https://api.github.com",
		"token":                   "",
		"repo":                    "octocat/hello-world",
		"branch":                  "main",
		"hookUser":                "foobar",
		"commitTriggerPhrase":     "",
		"commitDontpublishPhrase": "test",
	})

	var doiWriteAtts []map[string]string
	doiWriteAtts = append(doiWriteAtts, map[string]string{"attName": "type", "value": "DOI"})

	viper.SetDefault("WriteDOI", map[string]interface{}{"mode": "add",
		"parentPath":      "/TEI/teiHeader/fileDesc/publicationStmt",
		"elementType":     "idno",
		"position":        "lastChild",
		"attributeName":   "",
		"otherAttributes": doiWriteAtts,
		"addChangeDesc":   true,
	})

	viper.SetEnvPrefix("T2Z")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	// set configFile
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	viper.AddConfigPath("./")
	viper.AddConfigPath(os.Getenv("XDG_CONFIG_HOME") + "/t2z")
	viper.AddConfigPath(os.Getenv("HOME") + "/.t2z")
	viper.AddConfigPath("/etc/t2z/")

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			panic(fmt.Errorf("Config file not found"))
		}
		panic(fmt.Errorf("Cannot read config in %s: %+v", viper.ConfigFileUsed(), err))
	}

	/*
		viper.WatchConfig()
		viper.OnConfigChange(func(e fsnotify.Event) {
			fmt.Println("Config file changed:", e.Name)
		})
	*/

	err := viper.Unmarshal(Config)
	if err != nil {
		panic(fmt.Errorf("Cannot parse config: %+v", err))
	}

	Config.T2ZCommitMessage = "Zenodo DOI updated"

	return nil
}
